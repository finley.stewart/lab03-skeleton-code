package course.labs.activitylab

import android.os.Bundle
import android.app.Activity
import android.content.Intent
import android.content.Context
import android.util.Log
import android.view.Menu
import android.view.View
import android.widget.TextView

class ActivityOne : Activity() {

    // lifecycle counts
    private var createCount: Int = 0
    private var startCount: Int = 0
    private var resumeCount: Int = 0
    private var pauseCount: Int = 0
    private var stopCount: Int = 0
    private var restartCount: Int = 0
    private var destroyCount: Int = 0
    private lateinit var createView: TextView
    private lateinit var startView: TextView
    private lateinit var resumeView: TextView
    private lateinit var pauseView: TextView
    private lateinit var stopView: TextView
    private lateinit var restartView: TextView
    private lateinit var destroyView: TextView
    //TODO:  increment the variables' values when their corresponding lifecycle methods get called and log the info

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_one)

        createView = findViewById<TextView>(R.id.create) as TextView
        startView = findViewById<TextView>(R.id.start) as TextView
        resumeView = findViewById<TextView>(R.id.resume) as TextView
        pauseView = findViewById<TextView>(R.id.pause) as TextView
        stopView = findViewById<TextView>(R.id.stop) as TextView
        restartView = findViewById<TextView>(R.id.restart) as TextView
        destroyView = findViewById<TextView>(R.id.destroy) as TextView

        //Log cat print out
        Log.i(TAG, "onCreate called")

        retrieveInfo()
        createCount++
        update("All")

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_one, menu)
        return true
    }

    // lifecycle callback overrides

    public override fun onStart() {
        super.onStart()

        //Log cat print out
        Log.i(TAG, "onStart called")

        startCount++
        update("Start")
    }

    public override fun onResume() {
        super.onResume()

        Log.i(TAG, "onResume called")

        resumeCount++
        update("Resume")
    }

    public override fun onPause() {
        super.onPause()

        Log.i(TAG, "onPause called")

        pauseCount++
        update("Pause")
    }

    public override fun onStop() {
        super.onStop()

        Log.i(TAG, "onStop called")

        stopCount++
        saveInfo()
        update("Stop")
    }

    public override fun onRestart() {
        super.onRestart()

        Log.i(TAG, "onRestart called")

        restartCount++
        update("Restart")
    }

    public override fun onDestroy() {
        super.onDestroy()

        Log.i(TAG, "onDestroy called")

        destroyCount++
        update("Destroy")
    }

    // Note:  if you want to use a resource as a string you must do the following
    //  getResources().getString(R.string.stringname)   returns a String.

    public override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        Log.i(TAG, "onSaveInstanceState called")

        outState.putInt("createCount", createCount)
        outState.putInt("startCount", startCount)
        outState.putInt("resumeCount", resumeCount)
        outState.putInt("pauseCount", pauseCount)
        outState.putInt("stopCount", stopCount)
        outState.putInt("restartCount", restartCount)
        outState.putInt("destroyCount", destroyCount)
    }

    public override fun onRestoreInstanceState(inState: Bundle) {
        super.onRestoreInstanceState(inState)

        Log.i(TAG, "onRestoreInstanceState called")

        createCount = inState.getInt("createCount")
        startCount = inState.getInt("startCount")
        resumeCount = inState.getInt("resumeCount")
        pauseCount = inState.getInt("pauseCount")
        stopCount = inState.getInt("stopCount")
        restartCount = inState.getInt("restartCount")
        destroyCount = inState.getInt("destroyCount")

        update("All")
    }

    private fun update(view: String) {
        when(view) {
            "All"->{
                createView.setText(getString(R.string.onCreate) + createCount)
                startView.setText(getString(R.string.onStart) + startCount);
                resumeView.setText(getString(R.string.onResume) + resumeCount)
                pauseView.setText(getString(R.string.onPause) + pauseCount)
                stopView.setText(getString(R.string.onStop) + stopCount)
                restartView.setText(getString(R.string.onRestart) + restartCount)
                destroyView.setText(getString(R.string.onDestroy) + destroyCount)
            }
            "Create"-> createView.setText(getString(R.string.onCreate) + createCount)
            "Start"-> startView.setText(getString(R.string.onStart) + startCount)
            "Resume"-> resumeView.setText(getString(R.string.onResume) + resumeCount)
            "Pause"-> pauseView.setText(getString(R.string.onPause) + pauseCount)
            "Stop"-> stopView.setText(getString(R.string.onStop) + stopCount)
            "Restart"-> restartView.setText(getString(R.string.onRestart) + restartCount)
            "Destroy"-> destroyView.setText(getString(R.string.onDestroy) + destroyCount)
        }
    }

    private fun retrieveInfo() {
        val prefs = getPreferences(Context.MODE_PRIVATE)

        createCount = prefs.getInt(getString(R.string.onCreate), 0)
        startCount = prefs.getInt(getString(R.string.onStart), 0)
        resumeCount = prefs.getInt(getString(R.string.onResume), 0)
        pauseCount = prefs.getInt(getString(R.string.onPause), 0)
        stopCount = prefs.getInt(getString(R.string.onStop), 0)
        restartCount = prefs.getInt(getString(R.string.onRestart), 0)
        destroyCount = prefs.getInt(getString(R.string.onDestroy), 0)
    }

    private fun saveInfo() {
        with (getPreferences(Context.MODE_PRIVATE).edit()) {

            putInt(getString(R.string.onCreate), createCount)
            putInt(getString(R.string.onStart), startCount)
            putInt(getString(R.string.onResume), resumeCount)
            putInt(getString(R.string.onPause), pauseCount)
            putInt(getString(R.string.onStop), stopCount)
            putInt(getString(R.string.onRestart), restartCount)
            putInt(getString(R.string.onDestroy), destroyCount)

            apply()
        }
    }


    fun launchActivityTwo(view: View) {
        startActivity(Intent(this, ActivityTwo::class.java))
    }

    // singleton object in kotlin
    // companion object
    companion object {
        // string for logcat
        private val TAG = "Lab-ActivityOne"
    }


}
